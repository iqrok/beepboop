#!/bin/bash

base=2500
if [[ $6 -gt 0 ]]
then
	base=$6
fi

for(( i=0; i<$4; i++ ))
do
	total=$(($1*3))
	for (( c=0; c<=$(($total-1)); c++ ))
	do
		n=$(($c%$1))
		n=$(($n+1))
		freq=$(($2/$n))
		freq=$(($freq/7))
		l=$(($3/$total))

		if [[ $5 -gt 0 ]]
		then
			if [[ $(($c%2)) -gt 0 ]] 
			then
				freq=$(($base-$freq))
			else
				freq=$(($base+$freq))
			fi
		else
			freq=$(($base+$freq))
		fi

		beep -f $freq -l $l
	done
	sleep 1s
done
